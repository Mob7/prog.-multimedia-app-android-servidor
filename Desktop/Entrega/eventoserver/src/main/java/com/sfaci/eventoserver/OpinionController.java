package com.sfaci.eventoserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class OpinionController {

    @Autowired
    private OpinionRepository repository;

    @RequestMapping("/opiniones")
    public List<Opinion> getOpiniones(){
        List<Opinion> listaOpiniones = repository.findAll();
        return listaOpiniones;
    }

    @RequestMapping("/add_opinion")
    public void addOpinion(@RequestParam(value = "nombre", defaultValue = "nada") String nombre,
                           @RequestParam(value = "carretera" , defaultValue = "nada") String carretera,
                           @RequestParam(value = "localidad", defaultValue = "nada") String localidad,
                           @RequestParam(value = "texto", defaultValue = "nada") String texto) {

        Opinion opinion = new Opinion();
        opinion.setNombre(nombre);
        opinion.setCarretera(carretera);
        opinion.setLocalidad(localidad);
        opinion.setTexto(texto);

        repository.save(opinion);

    }
}
