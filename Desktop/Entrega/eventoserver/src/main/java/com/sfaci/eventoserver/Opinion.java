package com.sfaci.eventoserver;

import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * Opinion que los usuarios tienen sobre un monumento
 * Se deben definir las anotaciones que indican la tabla y columnas a las que
 * representa esta clase y sus atributos
 *
 * @author Santiago Faci
 * @version curso 2015-2016
 */
@Entity
@Table(name = "opinion")
public class Opinion {

    @Id
    @GeneratedValue
    private int id;
    @Column
    private String nombre;
    @Column
    private String carretera;
    @Column
    private String localidad;
    @Column
    private String texto;

    public Opinion(){

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarretera() {
        return carretera;
    }

    public void setCarretera(String carretera) {
        this.carretera = carretera;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
