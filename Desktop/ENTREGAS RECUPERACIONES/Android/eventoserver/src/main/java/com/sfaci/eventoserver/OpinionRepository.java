package com.sfaci.eventoserver;

import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface OpinionRepository extends CrudRepository<Opinion, Integer>{

    List<Opinion> findAll();

}

