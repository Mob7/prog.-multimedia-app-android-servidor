package com.example.sergio.plsstopaccidentes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sergio.plsstopaccidentes.database.DatabaseHandler;
import com.example.sergio.plsstopaccidentes.sources.Suggestion;

public class modificar_sugerencia extends AppCompatActivity {

    private DatabaseHandler db;

    private Button btnModify;
    private EditText etName;
    private EditText etRoad;
    private EditText etTown;
    private EditText etText;
    private int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_sugerencia);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        id = extras.getInt("id");

        db = new DatabaseHandler(getApplicationContext());

        btnModify = (Button) findViewById(R.id.btModificar);
        etName = (EditText) findViewById(R.id.editmodNombre);
        etRoad = (EditText) findViewById(R.id.editmodCarretera);
        etTown = (EditText) findViewById(R.id.editmodLocalidad);
        etText = (EditText) findViewById(R.id.editmodComentario);

        btnModify.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Suggestion sugg = new Suggestion(
                        etName.getText().toString(),
                        etRoad.getText().toString(),
                        etTown.getText().toString(),
                        etText.getText().toString()
                );

                db.updateSuggestion(sugg, id);
                Toast.makeText(getApplicationContext(),R.string.sug_modif, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(modificar_sugerencia.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

}
