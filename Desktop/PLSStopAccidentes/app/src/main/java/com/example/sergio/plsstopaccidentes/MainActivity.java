package com.example.sergio.plsstopaccidentes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button btListado = (Button)findViewById(R.id.btListadoDeAccidentes);
        btListado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, listado_accidentes.class);
                startActivity(intent);
            }
        });

        Button btEnviarSugerenciaSobreCarreteras = (Button)findViewById(R.id.btSugerenciasSobreLasCarreteras);
        btEnviarSugerenciaSobreCarreteras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, enviar_sugerencias_carreteras.class);
                startActivity(intent);
            }
        });

        Button btLeerSugrenciasSobreCarreteras = (Button)findViewById(R.id.btLeerTodasLasSugerenciasDeLosUsuarios);
        btLeerSugrenciasSobreCarreteras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, listado_sugerencias.class);
                startActivity(intent);
            }
        });

        Button btEnviarSugerenciasSobreLaApp = (Button)findViewById(R.id.btEnviaTusSugerenciasSobreLaApp);
        btEnviarSugerenciasSobreLaApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, enviar_sugerencias_app.class);
                startActivity(intent);
            }
        });

        Button btListadoDeMejorasParaLaApp = (Button)findViewById(R.id.btListadoDeMejorasParaLaApp);
        btListadoDeMejorasParaLaApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, listado_de_mejoras.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, quienes_somos.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
