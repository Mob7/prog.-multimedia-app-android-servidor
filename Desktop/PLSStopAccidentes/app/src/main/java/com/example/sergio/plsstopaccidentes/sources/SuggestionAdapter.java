package com.example.sergio.plsstopaccidentes.sources;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sergio.plsstopaccidentes.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergio on 29/05/2016.
 */
public class SuggestionAdapter extends ArrayAdapter<Suggestion> {

    private Context context;
    private int layoutId;
    private List<Suggestion> data;

    public SuggestionAdapter(Context context, int layoutId, List<Suggestion> data) {

        super(context, layoutId, data);

        this.context = context;
        this.layoutId = layoutId;
        this.data = data;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View row = view;
        ItemSuggestion item = null;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutId, parent, false);

            item = new ItemSuggestion();
            item.name = (TextView) row.findViewById(R.id.tvName);
            item.road = (TextView) row.findViewById(R.id.tvRoad);
            item.town = (TextView) row.findViewById(R.id.tvTown);
            item.text = (TextView) row.findViewById(R.id.tvText);

            row.setTag(item);
        }
        else
        {
            item = (ItemSuggestion) row.getTag();
        }

        Suggestion sugg = data.get(position);
        item.name.setText(sugg.getName());
        item.road.setText(sugg.getRoad());
        item.town.setText(sugg.getTown());
        item.text.setText(sugg.getText());

        return row;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Suggestion getItem(int posicion) {

        return data.get(posicion);
    }

    static class ItemSuggestion {

        TextView name;
        TextView road;
        TextView town;
        TextView text;
    }
}
