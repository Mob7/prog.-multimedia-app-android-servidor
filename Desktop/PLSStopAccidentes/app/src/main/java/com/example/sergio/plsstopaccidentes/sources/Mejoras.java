package com.example.sergio.plsstopaccidentes.sources;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sergio on 31/05/2016.
 */
public class Mejoras {

    private int    id;
    private String nombre;
    private String carretera;
    private String localidad;
    private String texto;

    public Mejoras(){
        this.id = -1;
    }

    public Mejoras(int id, String nombre, String carretera, String localidad, String texto) {
        this.id = id;
        this.nombre = nombre;
        this.carretera = carretera;
        this.localidad = localidad;
        this.texto = texto;
    }

    public Mejoras(@JsonProperty("nombre") String nombre, @JsonProperty ("carretera") String carretera, @JsonProperty ("localidad") String localidad, @JsonProperty ("texto") String texto) {
        this.id = -1;
        this.nombre = nombre;
        this.carretera = carretera;
        this.localidad = localidad;
        this.texto = texto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarretera() {
        return carretera;
    }

    public void setCarretera(String carretera) {
        this.carretera = carretera;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
