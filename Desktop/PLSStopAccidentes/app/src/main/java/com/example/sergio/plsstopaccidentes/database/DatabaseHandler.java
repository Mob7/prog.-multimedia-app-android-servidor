package com.example.sergio.plsstopaccidentes.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sergio.plsstopaccidentes.sources.Suggestion;

import java.util.ArrayList;

/**
 * Created by sergio on 29/05/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper
{
    private static final int DB_VERSION = 1;

    private static final String DB_NAME = "stopaccidentes";

    private static final String TABLE_SUGGESTION = "suggestion";

    // Nombre de las Columnas de Tabla user
    private static final String COL_ID = "id";
    private static final String COL_NAME = "name";
    private static final String COL_ROAD = "road";
    private static final String COL_TOWN = "town";
    private static final String COL_TEXT = "text";

    public DatabaseHandler(Context context) {

        super(context, DB_NAME, null, DB_VERSION);

    }

    // Creando las tablas
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_SUGGESTION + "(" +
                COL_ID + " INTEGER PRIMARY KEY," +
                COL_NAME + " TEXT," +
                COL_ROAD + " TEXT," +
                COL_TOWN + " TEXT," +
                COL_TEXT + " TEXT" + ")";
        db.execSQL(CREATE_USER_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // Record a new suggestion
    public void addSuggestion(Suggestion sugg) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_NAME, sugg.getName());
        values.put(COL_ROAD, sugg.getRoad());
        values.put(COL_TOWN, sugg.getTown());
        values.put(COL_TEXT, sugg.getText());

        db.insert(TABLE_SUGGESTION, null, values);
        db.close();
    }

    // Obtain all suggestions from db
    public ArrayList<Suggestion> getAllSuggestions() {

        ArrayList<Suggestion> suggestions = new ArrayList<Suggestion>();
        String selectQuery = "SELECT * FROM " + TABLE_SUGGESTION;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        while (cursor.moveToNext()) {

            Suggestion sugg = new Suggestion(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
            );
            suggestions.add(sugg);
        }

        cursor.close();
        db.close();

        return suggestions;
    }

    // Update a certain suggestion
    public void updateSuggestion(Suggestion sugg, int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues values = new ContentValues();

        values.put(COL_NAME, sugg.getName());
        values.put(COL_ROAD, sugg.getRoad());
        values.put(COL_TOWN, sugg.getTown());
        values.put(COL_TEXT, sugg.getText());

        String[] whereArgs = new String[]
                {
                        String.valueOf(id)
                };

        db.update(TABLE_SUGGESTION, values, "id = ?", whereArgs);

        db.close();
    }
}
