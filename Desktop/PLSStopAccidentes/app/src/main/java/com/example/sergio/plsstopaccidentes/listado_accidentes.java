package com.example.sergio.plsstopaccidentes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.usage.UsageEvents;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.example.sergio.plsstopaccidentes.sources.Accidents;
import com.example.sergio.plsstopaccidentes.sources.AccidentsAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class listado_accidentes extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView lvAccidents;
    private AccidentsAdapter adapter;
    private ArrayList<Accidents> listAccidents;
    private static final String url = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/incidencia/accidente.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_accidentes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listAccidents = new ArrayList<Accidents>();
        adapter = new AccidentsAdapter(this, R.layout.json_item, listAccidents);
        lvAccidents = (ListView)findViewById(R.id.lvListadoDeAccidentes);
        lvAccidents.setAdapter(adapter);
        lvAccidents.setOnItemClickListener(this);

        registerForContextMenu(lvAccidents);
    }

    private void cargarListaAccidentes() {

        TareaDescarga tarea = new TareaDescarga();
        tarea.execute(url);

    }

    @Override
    protected void onResume() {
        super.onResume();

        cargarListaAccidentes();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_accidents, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_see_map:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    private class TareaDescarga extends AsyncTask<String, Void, Void> {

            private boolean error = false;
            private ProgressDialog dialog;

            @Override
            protected Void doInBackground(String... urls) {

                InputStream is = null;
                String resultado = null;
                JSONObject json = null;
                JSONArray jsonArray = null;

                try {
                    // Conecta con la URL y obtenemos el fichero con los datos
                    HttpClient clienteHttp = new DefaultHttpClient();
                    HttpGet httpPost = new HttpGet(urls[0]);
                    HttpResponse respuesta = clienteHttp.execute(httpPost);
                    HttpEntity entity = respuesta.getEntity();
                    is = entity.getContent();

                    // Lee el fichero de datos y genera una cadena de texto como resultado
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    StringBuilder sb = new StringBuilder();
                    String linea = null;

                    while ((linea = br.readLine()) != null)
                        sb.append(linea + "\n");

                    is.close();
                    resultado = sb.toString();

                    json = new JSONObject(resultado);
                    jsonArray = json.optJSONArray("result");

                    String ano = null;
                    String tipo = null;
                    String direccion = null;
                    String razon = null;
                    String coordenadas = null;
                    Accidents accidente = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ano = jsonArray.getJSONObject(i).getString("year");
                        tipo = jsonArray.getJSONObject(i).getString("type");
                        direccion = jsonArray.getJSONObject(i).getString("firstAddress");
                        razon = jsonArray.getJSONObject(i).getString("reason");
                        coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                        coordenadas = coordenadas.substring(1, coordenadas.length() - 1);
                        String latlong[] = coordenadas.split(",");

                        accidente = new Accidents(ano, tipo, direccion, razon, Double.parseDouble(latlong[0]), Double.parseDouble(latlong[1]));
                        listAccidents.add(accidente);
                    }

                } catch (ClientProtocolException cpe) {
                    cpe.printStackTrace();
                    error = true;
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                    error = true;
                } catch (JSONException jse) {
                    jse.printStackTrace();
                    error = true;
                }

                return null;
            }

            /**
             * Método que se ejecuta si la tarea es cancelada antes de terminar
             */
            @Override
            protected void onCancelled() {
                super.onCancelled();
                adapter.clear();
                listAccidents = new ArrayList<Accidents>();
            }

            /**
             * Método que se ejecuta durante el progreso de la tarea
             *
             * @param progreso
             */
            @Override
            protected void onProgressUpdate(Void... progreso) {
                super.onProgressUpdate(progreso);

                adapter.notifyDataSetChanged();
            }

            /**
             * Método ejecutado automáticamente justo antes de lanzar la tarea en segundo plano
             */
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                dialog = new ProgressDialog(listado_accidentes.this);
                dialog.setTitle(R.string.mensaje_cargando);
                dialog.show();
            }

            /**
             * Método ejecutado automáticamente justo después de terminar la parte en segundo plano
             * Es la parte donde podemos interactuar con el UI para notificar lo sucedido al usuario
             *
             * @param resultado
             */
            @Override
            protected void onPostExecute(Void resultado) {
                super.onPostExecute(resultado);

                if (error) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.mensaje_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (dialog != null)
                    dialog.dismiss();

                adapter.notifyDataSetChanged();
            }

        }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == lvAccidents.INVALID_POSITION)
            return;
        Accidents accidente = listAccidents.get(position);
        Intent intent = new Intent(listado_accidentes.this, EventMapActivity.class);
        intent.putExtra("latitud", listAccidents.get(position).getLatitud());
        intent.putExtra("longitud", listAccidents.get(position).getLongitud());
        intent.putExtra("titulo", listAccidents.get(position).getType());
        startActivity(intent);
    }


    }
