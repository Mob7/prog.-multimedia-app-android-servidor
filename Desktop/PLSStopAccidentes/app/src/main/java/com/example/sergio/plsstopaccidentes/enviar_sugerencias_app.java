package com.example.sergio.plsstopaccidentes;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


public class enviar_sugerencias_app extends AppCompatActivity {

    private EditText nombre;
    private EditText carretera;
    private EditText localidad;
    private EditText texto;
    private Button enviar;
    private CheckBox checkBox;
    boolean seleccionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_sugerencias_app);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nombre = (EditText) findViewById(R.id.editNombreApp);
        carretera = (EditText) findViewById(R.id.editValoracion);
        localidad = (EditText) findViewById(R.id.editAppLocalidad);
        texto = (EditText) findViewById(R.id.editPropuestaApp);
        enviar = (Button) findViewById(R.id.btEnviarCorreo);
        checkBox = (CheckBox) findViewById(R.id.checkbox);

        checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked())
                    seleccionado = true;
                else
                    seleccionado = false;
            }
        });

        enviar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(seleccionado==false){
                    Toast.makeText(getApplicationContext(), R.string.fallocheckbox, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    enviarMejora();
                    //Toast.makeText(getApplicationContext(), R.string.mejoraenviada, Toast.LENGTH_SHORT).show();
                }
            }
        });}

        private void enviarMejora(){

            String name = nombre.getText().toString();
            String road = carretera.getText().toString();
            String local = localidad.getText().toString();
            String text = texto.getText().toString();

            WebService webService = new WebService();
            webService.execute(name, road, local, text);
        }

    private class WebService extends AsyncTask<String, Void, Void> {

        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForObject("http://192.168.1.128:8082" + "/add_opinion?nombre=" + params[0] + "&carretera=" + params[1] + "&localidad=" + params[2] + "&texto" + params[3], Void.class);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(enviar_sugerencias_app.this);
            dialog.setTitle(R.string.enviandomejora);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getApplicationContext(), R.string.mejoraenviada, Toast.LENGTH_SHORT).show();
            if (dialog != null)
                dialog.dismiss();
            Intent intent = new Intent(enviar_sugerencias_app.this, MainActivity.class);
            startActivity(intent);
        }
    }
    }
