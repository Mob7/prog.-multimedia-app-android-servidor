package com.example.sergio.plsstopaccidentes;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.LatLng;


public class EventMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Double latitud;
    private Double longitud;
    private String titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_map);

        Intent i = getIntent();
        latitud = i.getDoubleExtra("latitud", 0);
        longitud = i.getDoubleExtra("longitud", 0);
        titulo = i.getStringExtra("titulo");
        uk.me.jstott.jcoord.LatLng ubicacion = DeUMTSaLatLng(latitud, longitud, 'N', 30);
        this.latitud = ubicacion.getLat();
        this.longitud = ubicacion.getLng();

        MapsInitializer.initialize(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng posicion = new LatLng(latitud, longitud);
        System.out.print(latitud+", "+longitud);
        mMap.addMarker(new MarkerOptions().position(posicion).title(titulo));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(posicion));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17f));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitud, longitud))
                .title(titulo));
    }

    public uk.me.jstott.jcoord.LatLng DeUMTSaLatLng(double este, double oeste, char zonaLat, int zonaLong) {

        uk.me.jstott.jcoord.UTMRef utm = new uk.me.jstott.jcoord.UTMRef(este, oeste, 'N', 30);

        return utm.toLatLng();
    }
    public LatLng parseCoordenadas(String localizacion) {

        if ((!localizacion.startsWith("[")) || (!localizacion.endsWith("]")))
            return null;

        localizacion = localizacion.substring(1, localizacion.length() - 1);
        String coordenadas[] = localizacion.split(",");

        uk.me.jstott.jcoord.LatLng ubicacion = DeUMTSaLatLng(Double.parseDouble(coordenadas[0]),
                Double.parseDouble(coordenadas[1]), 'N', 30);

        return new LatLng(ubicacion.getLat(), ubicacion.getLng());
    }
}
