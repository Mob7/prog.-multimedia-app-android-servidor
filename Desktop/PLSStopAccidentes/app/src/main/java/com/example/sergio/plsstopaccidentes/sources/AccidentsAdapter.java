package com.example.sergio.plsstopaccidentes.sources;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sergio.plsstopaccidentes.R;

import java.util.List;

/**
 * Created by sergio on 29/05/2016.
 */
public class AccidentsAdapter extends ArrayAdapter<Accidents>{

    private Context context;
    private int layoutId;
    private List<Accidents> data;

    public AccidentsAdapter(Context context, int layoutId, List<Accidents> data) {

        super(context, layoutId, data);

        this.context = context;
        this.layoutId = layoutId;
        this.data = data;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View row = view;
        ItemAccident item = null;

        if (row == null)
        {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutId, parent, false);

            item = new ItemAccident();
            item.year = (TextView) row.findViewById(R.id.tvYear);
            item.type = (TextView) row.findViewById(R.id.tvType);
            item.direction = (TextView) row.findViewById(R.id.tvDirection);
            item.reason = (TextView) row.findViewById(R.id.tvReason);

            row.setTag(item);
        }
        else
        {
            item = (ItemAccident) row.getTag();
        }

        Accidents acc = data.get(position);
        item.year.setText(acc.getYear());
        item.type.setText(acc.getType());
        item.direction.setText(acc.getDirection());
        item.reason.setText(acc.getReason());

        return row;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Accidents getItem(int posicion) {

        return data.get(posicion);
    }

    static class ItemAccident {

        TextView year;
        TextView type;
        TextView direction;
        TextView reason;
    }
}
