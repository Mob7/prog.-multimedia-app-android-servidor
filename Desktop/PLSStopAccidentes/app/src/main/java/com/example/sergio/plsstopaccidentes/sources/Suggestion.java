package com.example.sergio.plsstopaccidentes.sources;

/**
 * Created by sergio on 29/05/2016.
 */
public class Suggestion {
    private int    id;
    private String name;
    private String road;
    private String town;
    private String text;

    public Suggestion(int id, String name, String road, String town, String text) {
        this.id = id;
        this.name = name;
        this.road = road;
        this.town = town;
        this.text = text;
    }

    public Suggestion(String name, String road, String town, String text) {
        this.id = -1;
        this.name = name;
        this.road = road;
        this.town = town;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
