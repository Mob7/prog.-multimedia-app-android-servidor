package com.example.sergio.plsstopaccidentes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.sergio.plsstopaccidentes.database.DatabaseHandler;
import com.example.sergio.plsstopaccidentes.sources.Suggestion;
import com.example.sergio.plsstopaccidentes.sources.SuggestionAdapter;

import java.util.ArrayList;
import java.util.List;

public class listado_sugerencias extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private SuggestionAdapter adapter;
    private List<Suggestion> suggestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_sugerencias);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        suggestions = new ArrayList<Suggestion>();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        suggestions = db.getAllSuggestions();
        adapter = new SuggestionAdapter(this, R.layout.suggestion_item, suggestions);
        ListView lvSuggestions = (ListView) findViewById(R.id.listView);
        lvSuggestions.setAdapter(adapter);
        lvSuggestions.setOnItemClickListener(listado_sugerencias.this);
        adapter.setNotifyOnChange(true);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {

        Intent i = new Intent(this, modificar_sugerencia.class);
        i.putExtra("id", suggestions.get(position).getId());
        startActivity(i);
    }
}
